** Settings ***
Documentation     mask.sh tests
...
...               A test suite to validate mask shell script.

Library           OperatingSystem
Library           Process
Library           String

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   mask.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -f
${MASK}   **MASKED**

*** Test Cases ***
with no option
    [Tags]    mask    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stderr}    -m argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    mask    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option missing
    [Tags]    mask    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -m   P4ssw0rd
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option with missing argument
    [Tags]    mask   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   -m   P4ssw0rd
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option argument with wrong file path
    [Tags]    mask   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/wrongFile.xml   -m   P4ssw0rd
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: file /tmp/wrongFile.xml does not exist!

[-m] option missing
    [Tags]    mask    simple
    Given Copy File   ${TESTS_DIR}/files/output-ref.xml   /tmp/output.xml
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -m argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-m] option with missing argument
    [Tags]    mask   simple
    Given Copy File   ${TESTS_DIR}/files/output-ref.xml   /tmp/output.xml
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-v] option
    [Tags]    mask    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    mask    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    mask    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-m] option: mask a value
    [Tags]    mask   simple
    Given Copy File   ${TESTS_DIR}/files/output-ref.xml   /tmp/output.xml
    And Pattern matches n timesin file   /tmp/output.xml    P4ssw0rd    4
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m   P4ssw0rd
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Masked variable [0]: 4 matches
    And Pattern matches n times in file   /tmp/output.xml    P4ssw0rd    0
    And Pattern matches n times in file   /tmp/output.xml    ${MASK}    4

[-m] option: mask a value that does not exist
    [Tags]    mask   simple
    Given Copy File   ${TESTS_DIR}/files/output-ref.xml   /tmp/output.xml
    And Pattern matches n timesin file   /tmp/output.xml    P4ssw0rdX    0
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m   P4ssw0rdX
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Masked variable [0]: 0 matches
    And Pattern matches n times in file   /tmp/output.xml    P4ssw0rdX    0
    And Pattern matches n times in file   /tmp/output.xml    ${MASK}    0

[-m] option: mask multiple values
    [Tags]    mask   simple
    Given Copy File   ${TESTS_DIR}/files/output-ref.xml   /tmp/output.xml
    And Pattern matches n timesin file   /tmp/output.xml    P4ssw0rd    4
    And Pattern matches n timesin file   /tmp/output.xml    fred    2
    And Pattern matches n timesin file   /tmp/output.xml    P4ssw0rdX    0
    And Pattern matches n timesin file   /tmp/output.xml    betty    2
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m   P4ssw0rd,fred,P4ssw0rdX,betty
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Masked variable [0]: 4 matches
    And Should Contain    ${result.stdout}    Masked variable [1]: 2 matches
    And Should Contain    ${result.stdout}    Masked variable [2]: 0 matches
    And Should Contain    ${result.stdout}    Masked variable [3]: 2 matches
    And Pattern matches n times in file   /tmp/output.xml    P4ssw0rd    0
    And Pattern matches n times in file   /tmp/output.xml    fred    0
    And Pattern matches n times in file   /tmp/output.xml    P4ssw0rdX    0
    And Pattern matches n times in file   /tmp/output.xml    betty    0
    And Pattern matches n times in file   /tmp/output.xml    ${MASK}    8

[-m] option: run twice with same value
    [Tags]    mask   simple
    Given Copy File   ${TESTS_DIR}/files/output-ref.xml   /tmp/output.xml
    And Pattern matches n timesin file   /tmp/output.xml    P4ssw0rd    4
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m   P4ssw0rd
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Masked variable [0]: 4 matches
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m   P4ssw0rd
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Masked variable [0]: 0 matches
    And Pattern matches n times in file   /tmp/output.xml    P4ssw0rd    0
    And Pattern matches n times in file   /tmp/output.xml    ${MASK}    4

[-m] option: run twice with different values
    [Tags]    mask   simple
    Given Copy File   ${TESTS_DIR}/files/output-ref.xml   /tmp/output.xml
    And Pattern matches n timesin file   /tmp/output.xml    P4ssw0rd    4
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m   P4ssw0rd
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Masked variable [0]: 4 matches
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m   fred
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Masked variable [0]: 2 matches
    And Pattern matches n times in file   /tmp/output.xml    P4ssw0rd    0
    And Pattern matches n times in file   /tmp/output.xml    fred    0
    And Pattern matches n times in file   /tmp/output.xml    ${MASK}    6

[-m] option: mask a value with escaped character /
    [Tags]    mask   simple
    Given Copy File   ${TESTS_DIR}/files/output-ref.xml   /tmp/output.xml
    And Pattern matches n timesin file   /tmp/output.xml    dir1/dir2/dir3    2
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m   dir1/dir2/dir3
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Masked variable [0]: 2 matches
    And Pattern matches n times in file   /tmp/output.xml    dir1/dir2/dir3    0
    And Pattern matches n times in file   /tmp/output.xml    ${MASK}    2

[-m] option: mask a value with escaped characters []/\.^$*
    [Tags]    mask   simple
    Given Copy File   ${TESTS_DIR}/files/output-ref.xml   /tmp/output.xml
    And Pattern matches n times in file   /tmp/output.xml    a/[]\\y.^$*/z    2
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/output.xml   -m   a/[]\\y.^$*/z
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Masked variable [0]: 2 matches
    And Pattern matches n times in file   /tmp/output.xml    a/[]\\y.^$*/z    0
    And Pattern matches n times in file   /tmp/output.xml    ${MASK}    2

*** Keywords ***
Pattern matches n times in file
    [Arguments]    ${file}    ${pattern}    ${n}
    ${lines} =    Grep File    ${file}    ${pattern}
    ${count} =    Get Count    ${lines}    ${pattern}
    Should Be Equal As Integers    ${n}    ${count}
